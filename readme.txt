﻿Требования к проекту "Уравнение Шредигера"

Требования к интерфейсу

1. При запуске программы перед пользователем открывается окно с заглавием "Schrodinger_equation", двумя кнопками в центре 
"Particle's passage through a potential barrier" и "A particle in a one-dimensional potential well". Внизу окошка расположена 
кнопка "quit".

2. При нажатии первой кнопки перед пользователем открывается окошко с линией ввода самого уравнения и двумя типами уравнения, между 
которыми можно выбрать, поставив галочку у соответвующего варианта: "E>0, the size of the potentional barrier is bounded"
								    "E>0, the barrier is infinite"
Ниже кнопочка "Ok".
Если был выбран вариант с конечным размером барьера, всплывает окошко, предлагающее ввести его параметры (Задаются как функция).
+ также кнопочкa "Ok".

3. При нажатии второй кнопки перед пользователем открывается окошко с линией ввода самого уравнения и двумя типами уравнения, между 
которыми можно выбрать, поставив галочку у соответвующего варианта: "E<0, the size of the potentional well is bounded"
								    "E<0, the well is infinite"
Ниже кнопочка "Ok".
Если был выбран вариант с конечным размером ямы, всплывает окошко, предлагающее ввести её параметры (Задаются как функция).
+ также кнопочкa "Ok".

4.После того, как все данные введены, происходит подсчет значений, после чего перед пользователем всплывает окошко
побольше, где указано решение задачи.

Остальные требования пока в разработке.