#include "mainwindow.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <QtCore>
#include <QColor>
#include <QWidget>
#include <QtGui>
#include <QInputDialog>
#include <QMessageBox>
#include <math.h>
#include <unistd.h>
#include <complex>

#define BARRIER_INFINITE 1
#define WELL_BOUNDED 2

int state = 0;
double coeff;
double E;
double U;
int time;
double x0;
double x1;
double w_width;
int plot_state = 0;

FILE *file;

///MyWidget - Класс, в котором происходит вся работа программы за исключением интерфейса, наследуется от QWidget.

class MyWidget : public QWidget
{
   public:
    ///paintEvent - основная функция для рисования.
    /** В цикле функция проверяет значение глобальной переменной plot_state.
     * Если оно равно 1, вызывает фунцию plot_the_graph - рисование графика.
     * Иначе - цикл продолжается.
    @param *event - объект типа QPaintEvent.*/
    virtual void paintEvent(QPaintEvent *event)
    {
        for (int l = 0; l<=20; l++)
        {
            if (plot_state == 1)
            {
                plot_the_graph(event, file);
                return;
            }
        }
    }
    ///plot_the_graph - функция, рисующая график.
    /** 1.Функция считывает точки из файла, который передается ей в качестве параметра.
     *  2.Ищет максимальный элемент по оси y для правильного масштабирования графика.
     *  3.Рисует оси и обозначения.
     *  4.Находит коеффициенты масштабирования.
     *  5.В цикле рисуются точки по координатам из файла и отрезки, их соединяющие, причем обрабатываются сразу оба случая :
     *  с потенциальным барьером и потенциальной ямой. Во втором случае рисуется только часть графика при x > 0.
     @param *event - объект типа QPaintEvent
     @param *file - файл, в который записываются координаты точек функции.*/

    virtual void plot_the_graph(QPaintEvent *event, FILE *file)
    {
        QPainter p(this);
        double XX[time+1], YY[time+1];
        //считывание точек из файла и рисование точек и отрезков, их соединяющих
        file = fopen ("file.txt","r");
        int k = 0;
        while(!feof(file))
        {
            if (2 != fscanf(file, "%lf%lf", &XX[k], &YY[k])) {
                break;
            }
            k++;
        }
        //поиск максимального элемента по оси y
        double max = YY[0];
        for (k=0; k<time; k++)
        {
            if(YY[k]>max) max = YY[k];
        }
        printf("max = %f \n", max);
        //поиск минимального элемента по оси y
        double min = YY[0];
        for (k=0; k<time; k++)
        {
            if(YY[k]<min) min = YY[k];
        }
        printf("min = %f \n", min);
        //нормируем
        for (k=0; k<time; k++)
        {
            YY[k] = YY[k]/max;
        }
        //поиск минимального нормированного элемента по оси y
        double min_norm = YY[0];
        for (k=0; k<time; k++)
        {
            if(YY[k]<min_norm) min_norm = YY[k];
        }
        printf("min = %f \n", min_norm);

        //Оси и обозначения
        p.setPen(QPen(QColor(00, 00, 66), 3));
//        p.drawLine(20, 690, 1550, 690); //x
//        p.drawLine(785, 690, 785 , 50); //y
        p.drawLine(20, 690, 1580, 690); //x
        p.drawLine(785, 690, 785, 50); //y
        p.drawLine(797, 75, 785, 50); //стрелочка y
        p.drawLine(773, 75, 785, 50); //стрелочка y
        p.drawLine(1560 ,678, 1580, 690); //стрелочка x
        p.drawLine(1560, 702, 1580, 690); //стрелочка x
        p.drawLine(1525,675,1545,650); //Х
        p.drawLine(1525,650,1545,675); //Х
        p.drawArc(728,40,40,44,0,-16*180); //пси
        p.drawLine(748, 35, 748, 100); //пси

        //масштабирование
        double m = abs(1530.0/(2*(x1)));
        double n = 640/(1 - min_norm);
//        printf("x1 - x0 = %f \n", x1-x0);
//        printf("m = %f n = %f \n", m, n);
        for (k = 1; k < time-1; k++)
        {
            if(XX[k]<0 && state == 1)
            {
                p.setPen(QPen(QColor(0xFF, 0x14, 0x93), 5));
                p.drawPoint(785 + m*XX[k], -n*YY[k] + 690+n*min_norm);
                printf("---XX[%d] = %f YY = %f XXprint = %f YYprint = %f \n", k, XX[k], YY[k]*max, 785 + m*XX[k], -n*YY[k] + 690+n*min_norm);
                p.drawPoint(785 + m*XX[k+1], -n*YY[k+1] + 690+n*min_norm);
                printf("---XX[%d] = %f YY = %f XXprint = %f YYprint = %f \n", k+1, XX[k+1], YY[k+1]*max,785 + m*XX[k+1], -n*YY[k+1] + 690+n*min_norm);
                p.setPen(QPen(QColor(0x99, 0x33, 0xFF), 3));
                p.drawLine(785 + m*XX[k], -n*YY[k] + 690+n*min_norm, 785 + m*XX[k+1], -n*YY[k+1] + 690+n*min_norm);

            }

            if(XX[k]>=0)
            {
                p.setPen(QPen(QColor(0xFF, 0x14, 0x93), 5));
                p.drawPoint(20 + 770 + m*XX[k-1], -n*YY[k-1] + 690+n*min_norm);
                printf("+++XX[%d] = %f YY = %f XXprint = %f YYprint = %f \n", k-1, XX[k-1], YY[k-1]*max, 20 + 785 + m*XX[k-1], -n*YY[k-1] + 690+n*min_norm);
                p.drawPoint(20 + 770 + m*XX[k], -n*YY[k] + 690+n*min_norm);
                printf("+++XX[%d] = %f YY = %f XXprint = %f YYprint = %f \n", k, XX[k], YY[k]*max, 20 + 785 + m*XX[k], -n*YY[k] + 690+n*min_norm);
                p.setPen(QPen(QColor(0x99, 0x33, 0xFF), 3));
                p.drawLine(20 + 770 + m*XX[k-1],-n*YY[k-1] + 690+n*min_norm, 20 + 770 + m*XX[k], -n*YY[k] + 690+n*min_norm);

            }

        }
        if (state == 1)
            p.drawLine(20 + 770 + m*XX[(time/2)], -n*YY[(time/2)] + 690+n*min_norm, 785 + m*XX[(time/2)+1], -n*YY[(time/2)+1] + 690+n*min_norm);

    }

    ///Runge_Kutta_step - функция, выполняющая итерации методом Рунге-Кутта
    /** Runge_Kutta_step обрабатывает сразу 2 массива с данными, тем самым две функции.
     * (В данном случае это основная функция Ψ и функция ξ - производная Ψ).
     @param y1[] - массив из элементов первой функции.
     @param y2[] - массив из элементов второй функции.
     @param x - значение по оси x в точке.
     @param step - шаг по оси x.*/
    double Runge_Kutta_step(double y1[],double y2[], double x, int i, double step)
    {
        double k1, k2, k3, k4;
            k1 = function_1( x, y1[i]);
            k2 = function_1( (x + step/2), y1[i] +  (step/2)* k1);
            k3 = function_1( (x + step/2), y1[i] +  (step/2)* k2);
            k4 = function_1( (x + step), y1[i] +  step * k3);
            y2[i+1] = y2[i]+(step/6)*(k1 + 2*k2 + 2*k3 + k4);

            k1 = function_2( x, y2[i]);
            k2 = function_2( (x + step/2), y2[i] +  (step/2)* k1);
            k3 = function_2( (x + step/2), y2[i] +  (step/2)* k2);
            k4 = function_2( (x + step), y2[i] +  step * k3);
            y1[i+1] = y1[i]+(step/6)*(k1 + 2*k2 + 2*k3 + k4);  //все кси

    }
    ///function_1 - функция, описывающая первое уравнение в системе из двух уравнений с производными первого порядка, получившейся из исходного уравнения
    /// с производной второго порядка путем замены переменной.
    /** В засисимости от типа задачи (прохождение частицы через барьер или частица в потенциальной яме) и от знака x функция возвращает соотвестсвующее
     * значение.
     @param x - значение по оси x в точке.
     @param psi - значение функции Ψ в точке. */
    double function_1 (double x, double psi)
    {
        if (state == 1)
        {
            if (x < 0)
                return -E * psi;
            else
                return (U - E)*psi;
        }
        if (state ==2)
        {
            if (x <= 0)
                return 0;
            if (x > w_width )
                return -E * psi;
            if (x <=w_width && x > 0)
                return (U - E)*psi;
        }

    }

    ///function_2 - функция, описывающая второе уравнение в системе из двух уравнений с производными первого порядка, получившейся из исходного уравнения
    /// с производной второго порядка путем замены переменной.
    /** Функция в обоих типах задачи возвращает значение функции ξ - производной Ψ.
     @param x - значение по оси x в точке.
     @param ksi - значение функции ξ в точке. */
    double function_2 (double x, double ksi)
    {
        return ksi;
    }

    ///Solving - основная функция, находящая решение системы уравнений и вызывающая все необходимые для этого, более мелкие функции.
    /** 1.В функции задаются начальные условия в зависимости от типа задачи.
     *  2.В цикле 2 раза вызывается функция Runge_Kutta_step для нахождения решения дейсвительной и мнимой частей функции Ψ.
     *  3.В файл записываются ее значения как квадратные корни из суммы квадратов действительной и мнимой частей и соответствующие им точки x.
     *  Аргументов у функции нет.*/
       void Solving()
       {
       double x[time+1];
       x[time] = x1;
       x[0] = x0;
       double step = (x[time]-x[0])/time;
       double PSI_real[time + 1], KSI_real[time + 1], PSI_image[time + 1], KSI_image[time + 1];
       if (state == 1)
       {
           PSI_real[0] = 0;
           KSI_real[0] = 2*(U-E);
           PSI_image[0] = 1;
           KSI_image[0] = 0;

       }
       if (state == 2)
       {
           PSI_real[0] = 1;
           KSI_real[0] = 2*(U-E);
           PSI_image[0] = 1;
           KSI_image[0] = 0;
       }
       for (int i=0; i<time; i++)
       {
           x[i+1] = x[i] + step;
           Runge_Kutta_step(KSI_real, PSI_real, x[i], i , step);
           Runge_Kutta_step(KSI_image, PSI_image, x[i], i , step);
       }
       file = fopen ("file.txt","w");
       for (int j = 0; j < time; j++)
       {
            fprintf(file,"%f   %f\n", x[j], sqrt( (PSI_real[j])*(PSI_real[j]) + (PSI_image[j])*(PSI_image[j]) ) );
       }
    }

    ///BI_chosen - функция, вызываемая сразу же после того, как пользователь выбирает прохождение частицы через потенциальный барьер.
    /** 1.В этой функции в глобальную переменную state кладется значение BARRIER_INFINITE, в дальнейшем много раз позволяющее программе
     *  определить выбранный тип задачи.
     *  2.Перед пользователем открывается окошко, предлагающее ввести значение высоты потенциального барьера типа double.
     *  3.Вызывается функция Solving, производящая дальнейшее численное решение задачи.
     *  4.Закрывается файл, который был открыт в Solving.
     *  Аргументов у функции нет.*/
    virtual void BI_chosen()
    {
        state = BARRIER_INFINITE;
        QInputDialog *dialog_b;
        double b_hight = dialog_b->getDouble(this, QString("The barrier's hight"), "Value:");
        U = b_hight;
        Solving();
        fclose(file);
    }

    ///WB_chosen - функция, вызываемая сразу же после того, как пользователь выбирает частицу в потенциальной яме.
    /** 1.В этой функции в глобальную переменную state кладется значение WELL_BOUNDED, в дальнейшем много раз позволяющее программе
     *  определить выбранный тип задачи.
     *  2.Перед пользователем сначала открывается окошко, предлагающее ввести значение высоты потенциальной ямы типа double,
     *  затем второе окошко для ширины ямы типа double, отсчитываемой от 0.
     *  3.Вызывается функция Solving, производящая дальнейшее численное решение задачи.
     *  4.Закрывается файл, который был открыт в Solving.
     *  Аргументов у функции нет.*/
    virtual void WB_chosen()
    {
        state = WELL_BOUNDED;
        double w_hight = QInputDialog::getDouble(this, QString("The well's hight"), "Value:");
        w_width = QInputDialog::getDouble(this, QString("The well's width"), "Value:");
        U = w_hight;
        Solving();
        fclose(file);
    }

};
///PlotWindow - класс, в котором описан интерфейс всплывающего окна для рисования графика.
PlotWindow::PlotWindow(QWidget *parent) : QDialog(parent)
{
    setMinimumSize(1600,800);
    setMaximumSize(1600,800);
    p_close = new QPushButton("&Quit");
    p_dependence = new QLabel("<h2><i><ins><u><strong>The dependence of Ψ-function on x</strong></u></ins></i></h2>");
    p_dependence->setAlignment(Qt::AlignCenter);
    p_dependence->setFixedHeight(40);
    p_close->setFixedHeight(30);

    QHBoxLayout *p_down = new QHBoxLayout;
    p_down->addSpacing(760);
    p_down->addWidget(p_close);
    p_down->addSpacing(760);

    QVBoxLayout *p_main = new QVBoxLayout;
    p_main->addWidget(p_dependence);
    plotWidget = new MyWidget;
    plotWidget->setFixedSize(1600,700);
    p_main->addWidget(plotWidget);
    p_main->addLayout(p_down);
    setLayout(p_main);
    setWindowTitle("Schredinger equation.The graph.");
    QObject::connect(p_close, &QPushButton::clicked, this, &PlotWindow::p_quit);
}

///p_quit - функция, обработывающая событие клика кнопки 'Quit'.
/** После нажатия кнопки quit программа завершается и выходит.*/
void PlotWindow::p_quit(bool clicked)
{
     exit(0);
}

///MainWindow - класс, в котором описан интерфейс главного окна программы.
MainWindow::MainWindow(QWidget *parent) : QDialog(parent)
{
    resize(1700,900);
    lbl = new QLabel("<h1><i><ins><u><strong>Schredinger equation.</strong></u></ins></i></h1>");
    lbl->setAlignment(Qt::AlignCenter);
    lbl_e = new QLabel("<h3><ins><i><u>Enter the equation :</u></i></ins></h3>");
    lbl_e->setAlignment(Qt::AlignCenter);
    line_1 = new QLineEdit;
    line_3 = new QLineEdit;
    lbl_1 = new QLabel("<big>d<sup>2</sup>Ψ/dx<sup>2</sup> +</big>");
    lbl_2 = new QLabel("<big> 2m/h<sup>2</sup>         *        (</big>");
    lbl_3 = new QLabel("U - ");
    lbl_4 = new QLabel("<big>) *  Ψ    =   0</big>");
    close = new QPushButton("&Quit");
    lbl_b = new QLabel("<h2><i><ins>Particle's passage through <br> a potential barrier</ins></i></h2>");
    lbl_w = new QLabel("<h2><i><ins>A particle in a one-dimensional <br> potential well</ins></i></h2>");
    barrier_inf = new QRadioButton("E>0, the barrier is infinite");
    well_bounded = new QRadioButton("E<0, the size of the potentional well is bounded");

    plot = new QPushButton("Plot");

    lbl_xmin = new QLabel("X_start = ");
    lbl_xmax = new QLabel("X_end = ");
    xmin = new QLineEdit;
    xmax = new QLineEdit;

    lbl_time = new QLabel("Number of parts = ");
    e_time = new QLineEdit;

    QHBoxLayout *xstart = new QHBoxLayout;
    xstart->addWidget(lbl_xmin);
    xstart->addWidget(xmin);

    QHBoxLayout *xend = new QHBoxLayout;
    xend->addWidget(lbl_xmax);
    xend->addWidget(xmax);

    QHBoxLayout *xtime = new QHBoxLayout;
    xtime->addWidget(lbl_time);
    xtime->addWidget(e_time);

    QVBoxLayout *begin = new QVBoxLayout;
    begin->addLayout(xstart);
    begin->addLayout(xend);
    begin->addLayout(xtime);
    begin->addSpacing(100);

    QHBoxLayout *begin_new = new QHBoxLayout;
    begin_new->addSpacing(700);
    begin_new->addLayout(begin);
    begin_new->addSpacing(700);

    QHBoxLayout *l = new QHBoxLayout;
    l->addSpacing(400);
    l->addWidget(line_1);  //коеффициент при первом слогаемом
    l->addWidget(lbl_1);  //д2фи по дх
    l->addWidget(lbl_2); // + второе слогаемое
    l->addWidget(lbl_3); // -
    l->addWidget(line_3); // U
    l->addWidget(lbl_4);  // )
    l->addSpacing(400);

    QVBoxLayout *up = new QVBoxLayout;
    up->addSpacing(170);
    up->addWidget(lbl);
    up->addSpacing(100);
    up->addWidget(lbl_e);
    up->addSpacing(30);
    up->addLayout(l);

    QHBoxLayout *buttons = new QHBoxLayout;
    buttons->addWidget(plot);
    buttons->addWidget(close);

    QHBoxLayout *down = new QHBoxLayout;
    down->addSpacing(600);
    down->addLayout(buttons);
    down->addSpacing(600);

    QVBoxLayout *left = new QVBoxLayout;
    left->addSpacing(50);
    left->addWidget(lbl_b);
    left->addWidget(barrier_inf);
    left->addSpacing(120);

    QVBoxLayout *right = new QVBoxLayout;
    right->addSpacing(50);
    right->addWidget(lbl_w);
    right->addWidget(well_bounded);
    right->addSpacing(120);

    QHBoxLayout *buttonslow = new QHBoxLayout;
    buttonslow->addSpacing(400);
    buttonslow->addLayout(left);
    myWidget = new MyWidget;
    buttonslow->addWidget(myWidget);
    buttonslow->addLayout(right);
    buttonslow->addSpacing(400);

    QVBoxLayout *main = new QVBoxLayout;
    main->addLayout(up);
    main->addLayout(buttonslow);
    main->addLayout(begin_new);
    main->addLayout(down);

    setLayout(main);
    setWindowTitle("Schredinger equation.");

    QObject::connect(line_1, SIGNAL(textChanged(QString)), this, SLOT(Text_1_changed(QString)));
    QObject::connect(line_3, SIGNAL(textChanged(QString)), this, SLOT(Text_3_changed(QString)));
    QObject::connect(close, &QPushButton::clicked, this, &MainWindow::quit);
    QObject::connect(barrier_inf, &QPushButton::clicked, this, &MainWindow::BI_Clicked);
    QObject::connect(well_bounded, &QPushButton::clicked, this, &MainWindow::WB_Clicked);
    QObject::connect(plot, &QPushButton::clicked, this, &MainWindow::PLOT_Clicked);
    QObject::connect(xmax, SIGNAL(textChanged(QString)), this, SLOT(Text_xmax_changed(QString)));
    QObject::connect(xmin, SIGNAL(textChanged(QString)), this, SLOT(Text_xmin_changed(QString)));
    QObject::connect(e_time, SIGNAL(textChanged(QString)), this, SLOT(Text_time_changed(QString)));
}

///Text_1_changed - обработка события написания/изменения текста в строке для коеффициента при d2Ψ/dx^2
/** Функция считывает строку и переводит её в тип double.
 @param QString str - введенная строка. */
void MainWindow::Text_1_changed(QString str)
{
    coeff = line_1->text().toDouble();
}

///Text_3_changed - обработка события написания/изменения текста в строке для энергии.
/** Функция считывает строку и переводит её в тип double.
 @param QString str - введенная строка. */
void MainWindow::Text_3_changed(QString str)
{
    E = line_3->text().toDouble();
}

///Text_xmin_changed - обработка события написания/изменения текста в строке для начального значения x.
/** Функция считывает строку и переводит её в тип double.
 @param QString str - введенная строка. */
void MainWindow::Text_xmin_changed(QString str)
{
    x0 = xmin->text().toDouble();
}

///Text_xmax_changed - обработка события написания/изменения текста в строке для конечного значения x.
/** Функция считывает строку и переводит её в тип double.
 @param QString str - введенная строка. */
void MainWindow::Text_xmax_changed(QString str)
{
    x1 = xmax->text().toDouble();
}

///Text_time_changed - обработка события написания/изменения текста в строке для количества делений отрезка [x_min ; x_max].
/** Функция считывает строку и переводит её в тип double.
 @param QString str - введенная строка. */
void MainWindow::Text_time_changed(QString str)
{
    time = e_time->text().toInt();
}

///PLOT_clicked - обработка события нажатия на кнопку plot.
/** 1.Функция скрывает некоторые виджеты главного окна для красоты.
 *  2.Создается и показывается новое окно PlotWindow для рисования графика.
 *  3.В глобальную переменную plot_state кладется значение 1, осведомляющее программу о том, что нужно нарисовать график.
 *  Аргументов у функции нет.*/
void MainWindow::PLOT_Clicked()
{
    plot->hide();
    close->hide();
    PlotWindow *plotwindow = new PlotWindow();
    plotwindow->show();
    plot_state = 1;
}

///quit - функция, обработывающая событие клика кнопки 'Quit'.
/** После нажатия кнопки quit программа завершается и выходит.*/
void MainWindow::quit(bool clicked)
{
         exit(0);
}

///BI_Clicked - функция, обработывающая событие клика кнопки 'E>0, the barrier is infinite'.
/** После нажатия кнопки вызывается функция BI_chosen() из класса MyWidget.
    Аргументов у функции нет. */
void MainWindow::BI_Clicked()
{
    myWidget->BI_chosen();
}

///WB_Clicked - функция, обработывающая событие клика кнопки 'E<0, the size of the potentional well is bounded'.
/** После нажатия кнопки вызывается функция WB_chosen() из класса MyWidget.
    Аргументов у функции нет. */
void MainWindow::WB_Clicked()
 {
    myWidget->WB_chosen();
 }
