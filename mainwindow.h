#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QLine>
#include <QString>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QPushButton>
#include <QWidget>
#include <QDialog>
#include <QRect>
#include <QInputDialog>
#include <fstream>


class MyWidget;


class PlotWindow : public QDialog
{
    Q_OBJECT

public:
    PlotWindow(QWidget *parent = 0);
private:
    QLabel *p_lbl;
    QLabel *p_dependence;
    QLabel *p_psi;
    QLabel *p_x0;
    QLabel *p_x1;
    QPushButton *p_close;
    MyWidget *plotWidget;
    void p_quit(bool clicked);

};


class MainWindow : public QDialog
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
private:
    QLabel *lbl;
    QLabel *lbl_1;
    QLabel *lbl_2;
    QLabel *lbl_3;
    QLabel *lbl_4;
    QLabel *lbl_e;
    QLineEdit *line_1;
    QLineEdit *line_2;
    QLineEdit *line_3;
    QPushButton *close;
    QLabel *lbl_b;
    QLabel *lbl_w;

    QRadioButton *barrier_bounded;
    QRadioButton *well_bounded;
    QRadioButton *barrier_inf;
    QRadioButton *well_inf;


    QPushButton *plot;
    QLabel *lbl_xmin;
    QLabel *lbl_xmax;
    QLabel *lbl_time;
    QLineEdit *xmin;
    QLineEdit *xmax;
    QLineEdit *e_time;


    MyWidget *myWidget;
    QInputDialog *dialog_b;
    QInputDialog *dialog_w;
    void quit(bool clicked);
    void BB_Clicked();
    void BI_Clicked();
    void WB_Clicked();
    void WI_Clicked();
    void PLOT_Clicked();
public slots:
    void Text_1_changed(QString);
    void Text_3_changed(QString);
    void Text_xmin_changed(QString);
    void Text_xmax_changed(QString);
    void Text_time_changed(QString);
};

#endif // MAINWINDOW_H
